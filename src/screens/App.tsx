import React from "react";

import { Debug } from "../components/Debug";
import { DisplayText } from "../components/DisplayText";
import { Layout } from "../components/Layout";
import { getConfiguration } from "../config";
import { ConferencesContainer } from "./Conferences";

export function App() {
  const { debug } = getConfiguration();

  return (
    <React.Fragment>
      {debug && <Debug />}

      <Layout.Header>
        <DisplayText>Coolferences</DisplayText>
      </Layout.Header>

      <Layout.Main>
        <ConferencesContainer />
      </Layout.Main>
    </React.Fragment>
  );
}
