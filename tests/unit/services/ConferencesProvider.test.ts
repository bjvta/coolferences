import { getConfiguration } from "../../../src/config";
import { ConferencesProvider } from "../../../src/services/ConferencesProvider";
import { ConferenceJsonFactory } from "../../factories/ConferenceJson";

describe("ConferencesProvider", () => {
  describe(".get", () => {
    it("calls the conferences API", () => {
      // Setup
      const { api: expectedUrl } = getConfiguration();
      const fakeResponse = { ok: true, json: () => Promise.resolve({}) };
      const fetchSpy = jest.fn().mockReturnValue(Promise.resolve(fakeResponse));

      // Exercise
      ConferencesProvider.get({ getter: fetchSpy });

      // Verify
      expect(fetchSpy).toBeCalledWith(expectedUrl);
    });

    it("parses API response into Conference objects", async () => {
      // Setup
      const fakeConferenceJson = ConferenceJsonFactory();
      const fakeApiJsonResponse = { January: [fakeConferenceJson] };
      const fakeResponse = { json: () => Promise.resolve(fakeApiJsonResponse), ok: true };
      const fetchStub = jest.fn().mockReturnValue(Promise.resolve(fakeResponse));

      // Exercise
      const result = await ConferencesProvider.get({ getter: fetchStub });

      // Verify
      expect(result[0].name).toBe(fakeConferenceJson.name);
      expect(result[0].hasCoc).toBeDefined();
      expect(result[0].cocUrl).toBeDefined();
      expect(result[0].hasCfp).toBeDefined();
      expect(result[0].cfpUrl).toBeDefined();
      expect(result[0].location).toBeDefined();
    });

    // TODO: throws the expected error when e.g. API fetch fails
  });
});
