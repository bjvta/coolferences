import React from "react";

import { shallow } from "enzyme";

import { ConferencesContainer } from "../../../src/screens/Conferences";

it("renders without crashing", () => {
  expect(() => shallow(<ConferencesContainer />)).not.toThrowError();
});
